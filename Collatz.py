#!/usr/bin/env python3

# ---------------------------
# projects/collatz/Collatz.py
# Copyright (C)
# Glenn P. Downing
# ---------------------------

# ------------
# collatz_read
# ------------


def collatz_read(s):
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]


# ------------
# collatz_eval
# ------------


def collatz_eval(i, j):
    memo = {}
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """

    def collatz_cycle(n):
        a = n
        c = 1
        # while not at the end of the cycle
        while n > 1:
            # if even, divide by 2
            if (n % 2) == 0:
                n = (n // 2)
            # if odd, multiply by 3 and add 1
            else:
                n = (3 * n) + 1
            if n in memo:
                memo[a] = c + memo[n]
                return c + memo[n]
            # add to cycle
            c += 1
        # return the cycle length
        memo[a] = c
        return c

    max_c = -1
    # for each value in the interval, get the cycle count and set the max
    for n in range(min(i, j), max(j + 1, i + 1)):
        max_c = max(collatz_cycle(n), max_c)
    return max_c

# -------------
# collatz_print
# -------------


def collatz_print(w, i, j, v):
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")

# -------------
# collatz_solve
# -------------


def collatz_solve(r, w):
    """
    r a reader
    w a writer
    """
    for s in r:
        i, j = collatz_read(s)
        v = collatz_eval(i, j)
        collatz_print(w, i, j, v)
